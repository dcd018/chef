default['default']['system_user'] = "ec2-user"
default['default']['system_group'] = "ec2-user"
default['default']['home_dir'] = "/home/#{default['default']['system_user']}"

default['network_topology']['deployment_key'] = "xxxxxxxxx"
