default['network_topology']['aws']['config']['credentials']['access_key_id'] = "xxxxxxxxx"
default['network_topology']['aws']['config']['credentials']['secret_access_key'] = "xxxxxxxxx"
default['network_topology']['aws']['config']['region'] = "us-east-1"
default['network_topology']['aws']['s3']['bucket_name'] = "bucket_name"
default['network_topology']['aws']['s3']['prefix'] = "prefix"

bucket_name = node['network_topology']['aws']['s3']['bucket_name']
prefix = node['network_topology']['aws']['s3']['prefix']
deployment_key = node['network_topology']['deployment_key']
credentials_prefix = "#{bucket_name}/#{prefix}/#{deployment_key}/.credentials"

default['network_topology']['aws']['s3']['credentials']['prefix'] = credentials_prefix
default['network_topology']['aws']['s3']['credentials']['nat_gateway'] = "#{credentials_prefix}/#{deployment_key}.pem"