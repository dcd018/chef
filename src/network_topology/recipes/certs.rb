#
# Cookbook:: network_topology
# Recipe:: certs
#
# Copyright:: 2017, The Authors, All Rights Reserved.

nat_gateway_domain = node[:network_topology][:nat_gateway][:domain]

easy_rsa_dir = "#{node['default']['home_dir']}/rsa"
easy_rsa_vars = node[:network_topology][:certs][:vars]

tun1_key_name = "tun1.#{nat_gateway_domain}"
tun1_clients = node[:network_topology][:certs][:clients]

package "install_openvpn" do
  package_name ["openvpn", "easy-rsa"]
  action :install
  notifies :create, "directory[create_easy_rsa_dir]", :immediately
end

directory "create_easy_rsa_dir" do
  path easy_rsa_dir
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0775"
  action :nothing
  notifies :run, "execute[copy_easy_rsa_scripts]", :immediately
end

execute "copy_easy_rsa_scripts" do
  user node[:default][:system_user]
  command "cp -rf /usr/share/easy-rsa/2.0/* #{easy_rsa_dir}"
  action :nothing
  notifies :create, "template[create_easy_rsa_vars_file]", :immediately
end

template "create_easy_rsa_vars_file" do
  path "#{easy_rsa_dir}/vars"
  source "vars.erb"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0544"
  variables (easy_rsa_vars)
  action :nothing
  notifies :run, "execute[source_easy_rsa_vars]", :immediately
end

execute "source_easy_rsa_vars" do
  cwd easy_rsa_dir
  user node[:default][:system_user]
  command "source ./vars && ./clean-all"
  action :nothing
  notifies :run, "execute[genertate_tls_cert]", :immediately
end

execute "genertate_tls_cert" do
  cwd easy_rsa_dir
  user node[:default][:system_user]
  command "openvpn --genkey --secret keys/#{tun1_key_name}.key"
  action :nothing
  notifies :run, "bash[genertate_server_ca]", :immediately
end

bash "genertate_server_ca" do
  cwd easy_rsa_dir
  user node[:default][:system_user]
  code <<-EOH
  source ./vars
  KEY_NAME=ca.#{nat_gateway_domain}
  KEY_CN=ca.#{nat_gateway_domain}
  ./build-ca --batch
  EOH
  action :nothing
  notifies :run, "bash[genertate_server_cert]", :immediately
end

bash "genertate_server_cert" do
  cwd easy_rsa_dir
  user node[:default][:system_user]
  code <<-EOH
  source ./vars
  KEY_NAME=server.#{nat_gateway_domain}
  KEY_CN=server.#{nat_gateway_domain}
  ./build-key-server --batch
  EOH
  action :nothing
  notifies :run, "bash[genertate_tun1_server_cert]", :immediately
end

bash "genertate_tun1_server_cert" do
  cwd easy_rsa_dir
  user node[:default][:system_user]
  code <<-EOH
  source ./vars
  KEY_NAME=server.#{tun1_key_name}
  KEY_CN=server.#{tun1_key_name}
  ./build-key-server --batch
  EOH
  action :nothing
  notifies :run, "ruby_block[generate_tun1_client_certs]", :immediately
end

ruby_block "generate_tun1_client_certs" do
  block do
    tun1_clients.each do |client|
      r = Chef::Resource::Bash.new("generate_tun1_client_cert_#{client}", run_context)
      r.cwd easy_rsa_dir
      r.user node[:default][:system_user]
      r.code <<-EOH
      source ./vars
      KEY_NAME=#{client}.#{tun1_key_name}
      KEY_CN=#{client}.#{tun1_key_name}
      ./build-key --batch
      EOH
      r.run_action(:run)
    end
  end
  action :nothing
  notifies :create, "directory[create_certs_dir]", :immediately
end

directory "create_certs_dir" do
  path "#{node['default']['home_dir']}/certs"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0775"
  action :create
  notifies :run, "bash[copy_certs]", :immediately
end

bash "copy_certs" do
  cwd "#{node['default']['home_dir']}/certs"
  user node[:default][:system_user]
  code <<-EOH
  cp #{easy_rsa_dir}/keys/ca.crt ./ca.#{nat_gateway_domain}.crt
  cp #{easy_rsa_dir}/keys/ca.key ./ca.#{nat_gateway_domain}.key
  cp #{easy_rsa_dir}/keys/server.#{nat_gateway_domain}.crt ./
  cp #{easy_rsa_dir}/keys/server.#{nat_gateway_domain}.key ./
  cp #{easy_rsa_dir}/keys/#{tun1_key_name}.key ./
  cp #{easy_rsa_dir}/keys/*.#{tun1_key_name}.crt ./
  cp #{easy_rsa_dir}/keys/*.#{tun1_key_name}.key ./
  EOH
  action :nothing
end
