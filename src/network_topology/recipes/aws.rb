#
# Cookbook:: network_topology
# Recipe:: aws
#
# Copyright:: 2017, The Authors, All Rights Reserved.

directory "create_aws_config_dir" do
  path "#{node['default']['home_dir']}/.aws"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0775"
  action :create
  notifies :create, "template[create_aws_config]", :immediately
end

template "create_aws_config" do
  path "#{node['default']['home_dir']}/.aws/config"
  source "aws.config.erb"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0600"
  variables({
    :aws_region => node[:network_topology][:aws][:config][:region]
  })
  action :nothing
  notifies :create, "template[create_aws_credentials]", :immediately
end

template "create_aws_credentials" do
  path "#{node['default']['home_dir']}/.aws/credentials"
  source "aws.credentials.erb"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0600"
  variables({
    :aws_access_key_id => node[:network_topology][:aws][:config][:credentials][:access_key_id],
    :aws_secret_access_key => node[:network_topology][:aws][:config][:credentials][:secret_access_key]
  })
  action :nothing
end