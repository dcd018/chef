class Chef::Recipe::NatGatewayConfig

  attr_accessor :import
  
  def initialize(services, overrides)
    @services = services
    @overrides = overrides
    @import = {}
    @export = {}
  end

  def is_match(subject, target)
    if defined?(subject) && defined?(target)
      if subject.is_a?(Hash) && target.is_a?(Hash)
        subject.each do |subjectKey, subjectVal|
          target.each do |targetKey, targetVal|
            return true if is_match(subject[subjectKey], target[targetKey])
          end
        end
      elsif subject.is_a?(Array) && target.is_a?(Array)
        subject.each_with_index do |subjectVal, subjectIndex|
          target.each_with_index do |targetVal, targetIndex|
            return true if is_match(subject[subjectIndex], target[targetIndex])
          end
        end
      else
        return true if subject == target
      end
    end
    return false
  end

  def parse_services
    @services.each do |service, components|
      if !@services[service].to_a.empty? && defined?(@import[service])
        @export[service] = {}
        components.each do |component| 
          if defined?(@import[service][component])
            @export[service][component] = @import[service][component]
          end
        end
      end
    end
  end

  def parse_overrides(subject, target, value)
    if defined?(subject[target]) && defined?(value)
      if subject[target].is_a?(Hash) && value.is_a?(Hash)
        value.each do |key, val|
          parse_overrides(subject[target], key, val)
        end
      elsif subject[target].is_a?(Array) && value.is_a?(Array)
        subject[target].each_with_index do |subjectVal, subjectIndex|
          value.each_with_index do |elmVal, elmIndex|
            if is_match(subject[target][subjectIndex], value[elmIndex])
              parse_overrides(subject[target], subjectIndex, value[elmIndex])
            end
          end
        end
      else
        subject[target] = value
      end
    end
  end

  def set_overrides
    @overrides.each do |service, component|
      parse_overrides(@export, service, component)
    end
  end

  def export
    parse_services
    set_overrides
    return @export
  end
end