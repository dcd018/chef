#
# Cookbook:: network_topology
# Recipe:: nat_gateway
#
# Copyright:: 2017, The Authors, All Rights Reserved.

nat_gateway_public_ip = node[:network_topology][:nat_gateway][:public_ip]
nat_gateway_domain = node[:network_topology][:nat_gateway][:domain]
nat_gateway_instance_id = node[:network_topology][:nat_gateway][:instance_id]
nat_gateway_ssh_user = node[:network_topology][:nat_gateway][:ssh_user]
nat_gateway_ssh_key_name = node[:network_topology][:nat_gateway][:ssh_key_name]
nat_gateway_ssh_key_s3_path = node[:network_topology][:aws][:s3][:credentials][:nat_gateway]
nat_gateway_ssh_key_pem = "#{node['default']['home_dir']}/.ssh/#{nat_gateway_ssh_key_name}"

easy_rsa_dir = "#{node['default']['home_dir']}/rsa"
certs_dir = "#{node['default']['home_dir']}/certs"
tun1_key_name = "tun1.#{nat_gateway_domain}"
tun1_clients = node[:network_topology][:certs][:clients]

package "nmap" do
  action :install
  notifies :create, "template[create_ec2_resolve_ssh_script]", :immediately
end

template "create_ec2_resolve_ssh_script" do
  path "#{node['default']['home_dir']}/ec2_resolve_ssh"
  source "ec2_resolve_ssh.erb"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0550"
  action :nothing
  notifies :create, "template[create_nat_gateway_config]", :immediately
end

template "create_nat_gateway_config" do
  source "pfsense.autodeploy.session.json.erb"
  path "#{node['default']['home_dir']}/pfsense.autodeploy.session.json"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0755"
  action :nothing
  notifies :run, "ruby_block[export_nat_gateway_config]", :immediately
end

ruby_block "export_nat_gateway_config" do
  block do

    services = node['network_topology']['nat_gateway']['session']['services']
    overrides = node['network_topology']['nat_gateway']['session']['overrides'].to_hash
    import = Chef::JSONCompat.parse(IO.read("#{node['default']['home_dir']}/pfsense.autodeploy.session.json"))

    overrides['system'] = overrides['system'] || {}
    overrides['system']['webgui'] = overrides['system']['webgui'] || {}
    overrides['openvpn']['openvpn-server'] = overrides['openvpn']['openvpn-server'] || []
    overrides['ca'] = overrides['ca'] || []
    overrides['cert'] = overrides['cert'] || []

    authorities = overrides['ca']
    certs = overrides['cert']
    ovpn_servers = overrides['openvpn']['openvpn-server']

    ca_ref = CertUtils.uniqid
    ca_cert = CertUtils.parse_cert("#{certs_dir}/ca.#{nat_gateway_domain}.crt")
    ca_key = CertUtils.parse_key("#{certs_dir}/ca.#{nat_gateway_domain}.key")

    server_ref = CertUtils.uniqid
    server_cert = CertUtils.parse_cert("#{certs_dir}/server.#{nat_gateway_domain}.crt")
    server_key = CertUtils.parse_key("#{certs_dir}/server.#{nat_gateway_domain}.key")

    tun1_server_index = ovpn_servers.index{|x| !x['vpnid'].nil? && x['vpnid'] == '1'}
    tun1_server_ref = CertUtils.uniqid
    tun1_server_cert = CertUtils.parse_cert("#{certs_dir}/server.#{tun1_key_name}.crt")
    tun1_server_key = CertUtils.parse_key("#{certs_dir}/server.#{tun1_key_name}.key")
    tun1_tls_key = CertUtils.parse_ta_key("#{certs_dir}/#{tun1_key_name}.key")
    tun1_clients = node['network_topology']['certs']['clients'].to_a

    if !ca_cert.nil? && !ca_key.nil? && !server_cert.nil? && !server_key.nil?
      ca_cert = {
        'refid' => ca_ref,
        'descr' => "ca.#{nat_gateway_domain}",
        'crt' => ca_cert,
        'prv' => ca_key,
        'serial' => CertUtils.next_serial(authorities)
      }
      authorities.push(ca_cert)

      server_cert = {
        'refid' => server_ref,
        'descr' => "server.#{nat_gateway_domain}",
        'type' => 'server',
        'caref' => ca_ref,
        'crt' => server_cert,
        'prv' => server_key,
      }
      certs.push(server_cert)
      overrides['system']['webgui']['ssl-certref'] = server_ref

      if !tun1_server_index.nil? && !tun1_server_cert.nil? && !tun1_server_key.nil? && !tun1_tls_key.nil?
        tun1_server_cert = {
          'refid' => tun1_server_ref,
          'descr' => "server.#{tun1_key_name}",
          'type' => 'server',
          'caref' => ca_ref,
          'crt' => tun1_server_cert,
          'prv' => tun1_server_key,
        }
        certs.push(tun1_server_cert)
        ovpn_servers[tun1_server_index]['caref'] = ca_ref
        ovpn_servers[tun1_server_index]['certref'] = tun1_server_ref
        ovpn_servers[tun1_server_index]['tls'] = tun1_tls_key

        if !tun1_clients.nil? && !tun1_clients.empty?
          tun1_clients.each do |client|
            tun1_client_cert = CertUtils.parse_cert("#{certs_dir}/#{client}.#{tun1_key_name}.crt")
            tun1_client_key = CertUtils.parse_key("#{certs_dir}/#{client}.#{tun1_key_name}.key")

            if !tun1_client_cert.nil? && !tun1_client_key.nil?
              tun1_client_cert = {
                'refid' => CertUtils.uniqid,
                'descr' => "#{client}.#{tun1_key_name}",
                'type' => "user",
                'caref' => ca_ref,
                'crt' => tun1_client_cert,
                'prv' => tun1_client_key
              }
              certs.push(tun1_client_cert)
            end
          end
        end
      end
    end

    overrides['ca'] = authorities
    overrides['cert'] = certs
    overrides['openvpn']['openvpn-server'] = ovpn_servers

    config = NatGatewayConfig.new(services, overrides)
    config.import = import
    node.run_state['nat_gateway_config'] = config.export
  end
  action :nothing
  notifies :create, "template[create_nat_gateway_session_file]", :immediately
end

template "create_nat_gateway_session_file" do
  source "pfsense.autodeploy.session.erb"
  path "#{node['default']['home_dir']}/pfsense.autodeploy.session"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0755"
  variables(
    lazy {
      {
        config: node.run_state['nat_gateway_config'].to_json
      }
    }
  )
  action :nothing
  notifies :create, "directory[create_shell_scripts_dir]", :immediately
end

directory "create_shell_scripts_dir" do
  path "#{node['default']['home_dir']}/shell_scripts"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0775"
  action :create
  notifies :create, "template[create_update_nat_gateway_shell_script]", :immediately
end

template "create_update_nat_gateway_shell_script" do
  source "update_nat_gateway.erb"
  path "#{node['default']['home_dir']}/shell_scripts/update_nat_gateway"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0755"
  action :nothing
  notifies :create, "template[create_transfer_nat_gateway_session_file_shell_script]", :immediately
end

template "create_transfer_nat_gateway_session_file_shell_script" do
  source "transfer_nat_gateway_session_file.erb"
  path "#{node['default']['home_dir']}/shell_scripts/transfer_nat_gateway_session_file"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0755"
  variables({
    :session_file => "#{node['default']['home_dir']}/pfsense.autodeploy.session"
  })
  action :nothing
  notifies :create, "template[create_restore_nat_gateway_session_shell_script]", :immediately
end

template "create_restore_nat_gateway_session_shell_script" do
  source "restore_nat_gateway_session.erb"
  path "#{node['default']['home_dir']}/shell_scripts/restore_nat_gateway_session"
  owner node[:default][:system_user]
  group node[:default][:system_group]
  mode "0755"
  action :nothing
  notifies :run, "bash[import_nat_gateway_ssh_key]", :immediately
end

bash "import_nat_gateway_ssh_key" do
  cwd "#{node['default']['home_dir']}/.ssh"
  user node[:default][:system_user]
  environment ({
    "HOME" => node[:default][:home_dir], 
    "USER" => node[:default][:system_user]
  })
  code <<-EOH
  aws s3 cp s3://#{nat_gateway_ssh_key_s3_path} #{nat_gateway_ssh_key_name}.base64
  cat #{nat_gateway_ssh_key_name}.base64 | base64 --decode > #{nat_gateway_ssh_key_name}.crypt
  aws kms decrypt --ciphertext-blob fileb://#{nat_gateway_ssh_key_name}.crypt --output text --query Plaintext | base64 --decode > #{nat_gateway_ssh_key_name}
  chmod 600 #{nat_gateway_ssh_key_name}
  EOH
  not_if { ::File.exist?(nat_gateway_ssh_key_pem) }
  action :nothing
  notifies :run, "bash[update_nat_gateway]", :immediately
end

bash "update_nat_gateway" do
  cwd node[:default][:home_dir]
  user node[:default_system_user]
  environment ({
    "HOME" => node[:default][:home_dir], 
    "USER" => node[:default][:system_user]
  })
  code <<-EOH
  ./ec2_resolve_ssh -i #{nat_gateway_instance_id} -u #{nat_gateway_ssh_user} -k #{nat_gateway_ssh_key_pem}
  cat ./shell_scripts/update_nat_gateway | ssh -o 'StrictHostKeyChecking=no' -i #{nat_gateway_ssh_key_pem}  -T #{nat_gateway_ssh_user}@#{nat_gateway_public_ip}
  EOH
  action :nothing
  notifies :run, "bash[transfer_nat_gateway_session_file]", :immediately
end

bash "transfer_nat_gateway_session_file" do
  cwd node[:default][:home_dir]
  user node[:default][:system_user]
  environment ({
    "HOME" => node[:default][:home_dir], 
    "USER" => node[:default][:system_user]
  })
  code <<-EOH
  ./ec2_resolve_ssh -i #{nat_gateway_instance_id} -u #{nat_gateway_ssh_user} -k #{nat_gateway_ssh_key_pem}
  cat ./shell_scripts/transfer_nat_gateway_session_file | sftp -o 'StrictHostKeyChecking=no' -i #{nat_gateway_ssh_key_pem} #{nat_gateway_ssh_user}@#{nat_gateway_public_ip}
  EOH
  action :nothing
  notifies :run, "bash[restore_nat_gateway_session]", :immediately
end

bash "restore_nat_gateway_session" do
  cwd node[:default][:home_dir]
  user node[:default_system_user]
  environment ({
    "HOME" => node[:default][:home_dir], 
    "USER" => node[:default][:system_user]
  })
  code <<-EOH
  ./ec2_resolve_ssh -i #{nat_gateway_instance_id} -u #{nat_gateway_ssh_user} -k #{nat_gateway_ssh_key_pem}
  cat ./shell_scripts/restore_nat_gateway_session | ssh -o 'StrictHostKeyChecking=no' -i #{nat_gateway_ssh_key_pem}  -T #{nat_gateway_ssh_user}@#{nat_gateway_public_ip}
  EOH
  action :nothing
  notifies :run, "execute[reboot_nat_gateway]", :immediately
end

execute "reboot_nat_gateway" do
  user node[:default][:system_user]
  environment ({
    "HOME" => node[:default][:home_dir], 
    "USER" => node[:default][:system_user]
  })
  command "aws ec2 reboot-instances --instance-ids #{nat_gateway_instance_id}"
  action :nothing
end