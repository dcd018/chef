#
# Cookbook:: head
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

package "install_epel" do
  package_name "epel-release"
  action :install
end

execute "enable_epel" do
  user "root"
  command "sed -i 's/enabled=0/enabled=1/g' /etc/yum.repos.d/epel.repo"
  action :run
end

execute "update_system" do
  user "root"
  command "yum update -y"
  action :run
end

include_recipe 'network_topology::aws'
include_recipe 'network_topology::certs'
include_recipe 'network_topology::nat_gateway'