require 'securerandom'
require "base64"

class Chef::Recipe::CertUtils

  def self.parse_cert(crt_path)
    if !crt_path.to_s.empty? && File.file?(crt_path)
      crt = IO.read(crt_path)
      if !crt.to_s.empty?
        crt = crt.match(/-----BEGIN CERTIFICATE-----(.*?)-----END CERTIFICATE-----/m).to_s
        return Base64.strict_encode64(crt).to_s
      end
    end
    return nil
  end

  def self.parse_key(key_path)
    if !key_path.to_s.empty? && File.file?(key_path)
      key = IO.read(key_path)
      if !key.to_s.empty?
        key = key.match(/-----BEGIN PRIVATE KEY-----(.*?)-----END PRIVATE KEY-----/m).to_s
        return Base64.strict_encode64(key).to_s
      end
    end
    return nil
  end

  def self.parse_ta_key(ta_key_path)
    if !ta_key_path.to_s.empty? && File.file?(ta_key_path)
      ta_key = IO.read(ta_key_path)
      if !ta_key.to_s.empty?
        return Base64.strict_encode64(ta_key).to_s
      end
    end
    return nil
  end

  def self.next_serial(certs)
    if !certs.to_a.empty?
      serials = []
      certs.each do |cert|
        serial = !cert['serial'].empty? ? cert['serial'].to_i : nil
        if !serial.nil?
          serials.push(serial)
        end
      end
      if !serials.empty?
        serials = serials.sort {|a,b| b <=> a}
        res = serials[0] + 1
        return res.to_s
      end 
    end
    return "1"
  end

  def self.uniqid
    return SecureRandom.uuid.gsub('-', '').slice(0,13)
  end
end